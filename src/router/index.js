import Vue from 'vue'
import Router from 'vue-router'
import TransactionIndex from '@/components/transactions/Index.vue'
import TransactionShow from '@/components/transactions/Show.vue'
// import PostsNew from '@/components/posts/New.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: TransactionIndex
    },
    {
      path: '/:id',
      name: 'Transactions.show',
      component: TransactionShow
    }
  ]
})
